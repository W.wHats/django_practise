from rest_framework.routers import DefaultRouter
from .views import RecordViewSet

#urlpatterns = [
#    path('information/', RecordViewSet.as_view({"get": "list"})),
#    path('information/<int:pk>', RecordViewSet.as_view({"get": "retrieve"}))
#]
router = DefaultRouter()

router.register(r'information', RecordViewSet, basename="record")
urlpatterns = router.urls
