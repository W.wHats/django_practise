from django.shortcuts import render, get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import viewsets
from .models import *
from .serializers import *


class RecordView(APIView):
    def get(self, request):
        records = Record.objects.all()
        serializer = RecordSerializer(records, many=True)
        return Response({"records": serializer.data})

    def post(self, request):
        record = request.data.get("records")
        serializer = RecordSerializer(data=record)
        if serializer.is_valid(raise_exception=True):
            records_saved = serializer.save()
            return Response({"ok": "all done"})


class RecordViewSet(viewsets.ModelViewSet):
    serializer_class = RecordSerializer

    queryset = Record.objects.all()
    """
    def list(self, request):
        queryset = Record.objects.all()
        serializer = RecordSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Record.objects.all()
        record = get_object_or_404(queryset, pk=pk)
        serializer = RecordSerializer(record)
        return Response(serializer.data)
    """